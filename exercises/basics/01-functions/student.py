# De opdracht bestaat eruit om de gegeven Java-code
# te vertalen naar Python.
# Om je wat te helpen wordt er telkens een voorbeeldvertaling
# meegeleverd zodat je een idee hebt van hoe de oplossing
# eruit moet zien.

# 1. Zoek naar het .java bestand in de opgavedirectory en open het.
#    Dit bestand bevat de te vertalen code.
   
# 2. Open het bestand student.py.
#    Hierin moet je de Python vertaling schrijven.
   
# 3. Run de tests in de shell (rt).


# Voorbeeld
def increment(x):
    return x + 1


def square(x):
    # Vervang onderstaande lijn door de vertaling van de Java-code
    raise NotImplementedError()


def are_ordered(x, y, z):
    raise NotImplementedError()


# is_divisible_by
