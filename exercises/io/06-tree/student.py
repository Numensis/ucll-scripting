def tree(path = '.'):
    # Zie readme.txt
    raise NotImplementedException()


# Indien opgeroepen vanuit shell,
# moet het resultaat van tree(path)
# afprinten, waarbij path meegegeven
# wordt als command line argument.
# Indien er geen command line argument
# werd meegegeven, moet de tree
# van de huidige directory ('.')
# afgeprint worden.
if __name__ == '__main__':
    raise NotImplementedException()
