from urllib.request import urlopen
import re
import sys

for url in [ "https://rooster.ucll.be/nl/2017/studenten/afdeling/536CDF44C7C4597E25E54AD2ECCC6D49/week1/24/week2/39/groep/998BCFAA932F126AA0CACC7F16E9C9DA" ]:
    html = str(urlopen(url).read())
    matches = re.findall(r'<td class="cel">(MBI.*?)</td>', html)

    print(set(matches))
